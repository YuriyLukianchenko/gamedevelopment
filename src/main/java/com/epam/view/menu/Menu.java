package com.epam.view.menu;

import com.epam.model.units.*;


import com.epam.view.menu.defaults.DefaultMenuMessages;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Prints initial menu in console
 */
public class Menu {
    public List<Unit> ArrayOfUnits1 = new ArrayList();
    public List<Unit> ArrayOfUnits2 = new ArrayList();
    DefaultMenuMessages defaultMenuMessages = new DefaultMenuMessages();
    Scanner scan = new Scanner(System.in);

    int fromUser;

    public void printMenu() {
        defaultMenuMessages.printMainMenu();
        fromUser = scan.nextInt();

        // Loops until user enters right number
        while ((fromUser != 1) || (fromUser != 2) || (fromUser != 3)) {
            if (fromUser == 1) {
                // Prints "selecting units" menu
                teamSelecting();
                break;
            } else if (fromUser == 2) {
                /*
                 * Prints "About" window in console
                 * If user enters 9 returns him back to main menu
                 */
                defaultMenuMessages.printAbout();
                fromUser = scan.nextInt();
                if (fromUser == 9){
                    printMenu();
                }
            } else if (fromUser == 3) {
                return;
            } else {
                defaultMenuMessages.printMainMenuError();
            }
            fromUser = scan.nextInt();
        }
    }

    /**
     * This class provides function of team selecting
     */
    public void teamSelecting() {
        //After user chooses units, their icons are saved here
        ArrayList<String> unitsArr = unitsDefaultValue();

        defaultMenuMessages.printTeam1Selecting(unitsArr.get(0), unitsArr.get(1),
                unitsArr.get(2), unitsArr.get(3), unitsArr.get(4));

        // The amount of units in team = 5
        int counter = 0;
        String fromUser;

        // While there are free spaces in team
        while (counter < 5) {
            fromUser = scan.next();
            if (fromUser.equals("1")) {
                unitsArr.set(counter, defaultMenuMessages.SWORD_SIGN);
                ArrayOfUnits1.add(new DamageDealer());
            } else if (fromUser.equals("2")) {
                unitsArr.set(counter, defaultMenuMessages.HELMET_WITH_CROSS_SIGN);
                ArrayOfUnits1.add(new Healer());
            } else if (fromUser.equals("3")) {
                unitsArr.set(counter, defaultMenuMessages.MAGE_SIGN);
                ArrayOfUnits1.add(new Magician());
            } else if (fromUser.equals("4")) {
                unitsArr.set(counter, defaultMenuMessages.SHIELD_SIGN);
                ArrayOfUnits1.add(new Supporter());
            } else if (fromUser.equals("5")) {
                unitsArr.set(counter, defaultMenuMessages.FLEXED_BICEPS_SIGN);
                ArrayOfUnits1.add(new Tank());
            } else if (fromUser.equals("9")) {
                printMenu();
            } else {
                continue;
            }
            counter++;
            defaultMenuMessages.printTeam1Selecting(unitsArr.get(0), unitsArr.get(1),
                    unitsArr.get(2), unitsArr.get(3), unitsArr.get(4));
        }

        unitsArr = unitsDefaultValue();
        defaultMenuMessages.printTeam2Selecting(unitsArr.get(0), unitsArr.get(1),
                unitsArr.get(2), unitsArr.get(3), unitsArr.get(4));
        counter = 0;
        while (counter < 5) {
            fromUser = scan.next();
            if (fromUser.equals("1")) {
                unitsArr.set(counter, defaultMenuMessages.SWORD_SIGN);
                ArrayOfUnits2.add(new DamageDealer());
            } else if (fromUser.equals("2")) {
                unitsArr.set(counter, defaultMenuMessages.HELMET_WITH_CROSS_SIGN);
                ArrayOfUnits2.add(new Healer());
            } else if (fromUser.equals("3")) {
                unitsArr.set(counter, defaultMenuMessages.MAGE_SIGN);
                ArrayOfUnits2.add(new Magician());
            } else if (fromUser.equals("4")) {
                unitsArr.set(counter, defaultMenuMessages.SHIELD_SIGN);
                ArrayOfUnits2.add(new Supporter());
            } else if (fromUser.equals("5")) {
                unitsArr.set(counter, defaultMenuMessages.FLEXED_BICEPS_SIGN);
                ArrayOfUnits2.add(new Tank());
            } else if (fromUser.equals("9")) {
                printMenu();
            } else {
                continue;
            }
            counter++;
            defaultMenuMessages.printTeam2Selecting(unitsArr.get(0), unitsArr.get(1),
                    unitsArr.get(2), unitsArr.get(3), unitsArr.get(4));
        }

        /**
         * Prints "Ready" text in console with interval of 1 second
         */
        defaultMenuMessages.printReady3();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        defaultMenuMessages.printReady2();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        defaultMenuMessages.printReady1();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Default values for ArrayList in method <p>teamSelecting()</p>
     *
     * @return
     */
    public ArrayList<String> unitsDefaultValue() {
        ArrayList<String> res = new ArrayList<>();
        res.add("1");
        res.add("2");
        res.add("3");
        res.add("4");
        res.add("5");
        return res;
    }
}
