package com.epam.view.menu.defaults;

/**
 * This class contains console menu messages
 */
public class DefaultMenuMessages {
    public static final String BORDER = "\u2588";
    public static final String SHIELD_SIGN = "\u26E8";
    public static final String HELMET_WITH_CROSS_SIGN = "\u26D1";
    public static final String MAGE_SIGN = "\uD83E\uDDD9";
    public static final String SWORD_SIGN = "\ud83d\udde1";
    public static final String FLEXED_BICEPS_SIGN = "\uD83D\uDCAA";

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public void printMainMenu() {
        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██             " +
                        ANSI_YELLOW + "       ███╗   ███╗███████╗███╗   ██╗██╗   ██╗    " + ANSI_RESET + "               ██\n" +
                        "██             " +
                        ANSI_YELLOW + "       ████╗ ████║██╔════╝████╗  ██║██║   ██║    " + ANSI_RESET + "               ██\n" +
                        "██              " +
                        ANSI_YELLOW + "      ██╔████╔██║█████╗  ██╔██╗ ██║██║   ██║      " + ANSI_RESET + "             ██\n" +
                        "██              " +
                        ANSI_YELLOW + "      ██║╚██╔╝██║██╔══╝  ██║╚██╗██║██║   ██║      " + ANSI_RESET + "             ██\n" +
                        "██              " +
                        ANSI_YELLOW + "      ██║ ╚═╝ ██║███████╗██║ ╚████║╚██████╔╝     " + ANSI_RESET + "              ██\n" +
                        "██              " +
                        ANSI_YELLOW + "      ╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝ ╚═════╝      " + ANSI_RESET + "              ██\n" +
                        "██                                                                             ██\n" +
                        "██           __                                                                ██\n" +
                        "██   /|     (_ _|_  _. ._ _|_    _.    _   _. ._ _   _                         ██\n" +
                        "██    | o   __) |_ (_| |   |_   (_|   (_| (_| | | | (/_                        ██\n" +
                        "██                                     _|                                      ██\n" +
                        "██   _                                                                         ██\n" +
                        "██    )      /\\  |_   _     _|_                                                ██\n" +
                        "██   /_ o   /--\\ |_) (_) |_| |_                                                ██\n" +
                        "██                                                                             ██\n" +
                        "██   _       _                                                                 ██\n" +
                        "██   _)     / \\     o _|_                                                      ██\n" +
                        "██   _) o   \\_X |_| |  |_                                                      ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );

    }

    public void printMainMenuError() {
        System.out.print("\n" +
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                "██                                                                             ██\n" +
                "██                                                                             ██\n" +
                "██             " +
                ANSI_YELLOW + "      ███╗   ███╗███████╗███╗   ██╗██╗   ██╗     " + ANSI_RESET + "               ██\n" +
                "██             " +
                ANSI_YELLOW + "      ████╗ ████║██╔════╝████╗  ██║██║   ██║     " + ANSI_RESET + "               ██\n" +
                "██              " +
                ANSI_YELLOW + "     ██╔████╔██║█████╗  ██╔██╗ ██║██║   ██║       " + ANSI_RESET + "             ██\n" +
                "██              " +
                ANSI_YELLOW + "     ██║╚██╔╝██║██╔══╝  ██║╚██╗██║██║   ██║       " + ANSI_RESET + "             ██\n" +
                "██              " +
                ANSI_YELLOW + "     ██║ ╚═╝ ██║███████╗██║ ╚████║╚██████╔╝      " + ANSI_RESET + "              ██\n" +
                "██              " +
                ANSI_YELLOW + "     ╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝ ╚═════╝       " + ANSI_RESET + "              ██\n" +
                "██           __                                                                ██\n" +
                "██   /|     (_ _|_  _. ._ _|_    _.    _   _. ._ _   _                         ██\n" +
                "██    | o   __) |_ (_| |   |_   (_|   (_| (_| | | | (/_                        ██\n" +
                "██                                     _|                                      ██\n" +
                "██   _                                                                         ██\n" +
                "██    )      /\\  |_   _     _|_                                                ██\n" +
                "██   /_ o   /--\\ |_) (_) |_| |_                                                ██\n" +
                "██                                                                             ██\n" +
                "██   _       _                                                                 ██\n" +
                "██   _)     / \\     o _|_                                                      ██\n" +
                "██   _) o   \\_X |_| |  |_                                                      ██\n" +
                "██                                                                             ██\n" +
                "██                                                                             ██\n" +
                "██ ");
        System.out.print(ANSI_RED +
                "                YOU ENTERED WRONG NUMBER, PLEASE TRY AGAIN                  " +
                ANSI_RESET);
        System.out.print(
                "██\n██                                                                             ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████");
    }

    public void printAbout() {
        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██            " +
                        ANSI_CYAN + "               ____   ____  _    _ _______     " + ANSI_RESET + "                  ██\n" +
                        "██            " +
                        ANSI_CYAN + "         /\\   |  _ \\ / __ \\| |  | |__   __| " + ANSI_RESET + "                     ██\n" +
                        "██            " +
                        ANSI_CYAN + "        /  \\  | |_) | |  | | |  | |  | |      " + ANSI_RESET + "                   ██\n" +
                        "██            " +
                        ANSI_CYAN + "       / /\\ \\ |  _ <| |  | | |  | |  | |     " + ANSI_RESET + "                    ██\n" +
                        "██            " +
                        ANSI_CYAN + "      / ____ \\| |_) | |__| | |__| |  | |      " + ANSI_RESET + "                   ██\n" +
                        "██            " +
                        ANSI_CYAN + "     /_/    \\_\\____/ \\____/ \\____/   |_|   " + ANSI_RESET + "                      ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██  This is a medieval-style PVP strategy game.                                ██\n" +
                        "██  There are 5 kinds of units: warrior, healler, magician, supporter, and     ██\n" +
                        "██  tank. Each unit has different amount of hp and armor. Also each unit has   ██\n" +
                        "██  a special ability: healler - heals choosen unit, supporter - gives         ██\n" +
                        "██  additional atack, tank - gives armor. Each player can do one action        ██\n" +
                        "██  per turn - to heal/ to attack and so on.                                   ██\n" +
                        "██  Each player task is to kill all. As soon as it's done - player wins.       ██\n" +
                        "██  .                                                                          ██\n" +
                        "██  .                                                                          ██\n" +
                        "██  .                                                                          ██\n" +
                        "██                                                                             ██\n" +
                        "██     Creared by EPAM students:   Taras Babii, Yuriy Lukianchenko             ██\n" +
                        "██                                 Andriy Shymoniak, Serhii Bubnii             ██\n" +
                        "██                                                                             ██\n" +
                        "██                            Press 9 to exit                                  ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    public void printHorizontalBorder() {
        for (int i = 0; i < 80; i++) {
            System.out.print(BORDER);
        }
    }

    /**
     * Prints menu, where user can choose which units to use in the battle
     * <p>
     * These variables are needed to change icons in selecting bar after user
     * adds a unit into his team. This makes team choosing easier to understand
     *
     * @param par1
     * @param par2
     * @param par3
     * @param par4
     * @param par5
     */
    public void printTeamSelecting(String par1, String par2, String par3, String par4, String par5) {

        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██     " +
                        ANSI_RED + "  ________  _______ __________   ___     _____________   __  ___ " + ANSI_RESET + "       ██\n" +
                        "██     " +
                        ANSI_RED + " / ___/ _ \\/ __/ _ /_  __/ __/  / _ |   /_  __/ __/ _ | /  |/  /" + ANSI_RESET + "        ██\n" +
                        "██     " +
                        ANSI_RED + "/ /__/ , _/ _// __ |/ / / _/   / __ |    / / / _// __ |/ /|_/ / " + ANSI_RESET + "        ██\n" +
                        "██     " +
                        ANSI_RED + "\\___/_/|_/___/_/ |_/_/ /___/  /_/ |_|   /_/ /___/_/ |_/_/  /_/ " + ANSI_RESET + "         ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██   " +
                        "1 - Warrior " + SWORD_SIGN + "  [100 hp][20 attack][5 defence][30 damage]\t               ██\n" +
                        "██       *special ability* - super kick...                             \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par1 + "_|  ██\n" +
                        "██   " +
                        "2 - Healler " + HELMET_WITH_CROSS_SIGN + "  [80 hp][5 attack][1 defence][5 damage]\t\t               ██\n" +
                        "██       *special ability* - heals ...         \t\t               \t\t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par2 + "_|  ██\n" +
                        "██   " +
                        "3 - Magician " + MAGE_SIGN + "  [80 hp][10 attack][1 defence][10 damage]            \t   ██\n" +
                        "██       *special ability* - poison...                                 \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par3 + "_|  ██\n" +
                        "██   " +
                        "4 - Supporter " + SHIELD_SIGN + "  [100 hp][10 attack][3 defence][10 damage]\t\t       ██\n" +
                        "██        *special ability* - gives ...                                \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par4 + "_|  ██\n" +
                        "██   " +
                        "5 - Tank " + FLEXED_BICEPS_SIGN + "\t [150 hp][10 attack][10 defence][15 damage]\t               ██\n" +
                        "██       *special ability* -                                          \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par5 + "_|  ██\n" +
                        "██   Enter numbers of units you want to add to your team                       ██\n" +
                        "██                            Press 9 to exit                                  ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    @SuppressWarnings("Duplicates")
    public void printTeam1Selecting(String par1, String par2, String par3, String par4, String par5){

        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██" +
                        ANSI_BLUE + "   ________  _______ __________   ___     _____________   __  ___     ____   " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_BLUE + "  / ___/ _ \\/ __/ _ /_  __/ __/  / _ |   /_  __/ __/ _ | /  |/  /    /  _/   " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_BLUE + " / /__/ , _/ _// __ |/ / / _/   / __ |    / / / _// __ |/ /|_/ /     / /     " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_BLUE + " \\___/_/|_/___/_/ |_/_/ /___/  /_/ |_|   /_/ /___/_/ |_/_/  /_/    _/ /      " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_BLUE + "                                                                  /___/      " + ANSI_RESET + "██\n" +
                        "██                                                                             ██\n" +
                        "██   " +
                        "1 - Warrior " + SWORD_SIGN + "  [100 hp][20 attack][5 defence][30 damage]\t               ██\n" +
                        "██       *special ability* - super kick...                             \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par1 + "_|  ██\n" +
                        "██   " +
                        "2 - Healler " + HELMET_WITH_CROSS_SIGN + "  [80 hp][5 attack][1 defence][5 damage]\t\t               ██\n" +
                        "██       *special ability* - heals ...         \t\t               \t\t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par2 + "_|  ██\n" +
                        "██   " +
                        "3 - Magician " + MAGE_SIGN + "  [80 hp][10 attack][1 defence][10 damage]            \t   ██\n" +
                        "██       *special ability* - poison...                                 \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par3 + "_|  ██\n" +
                        "██   " +
                        "4 - Supporter " + SHIELD_SIGN + "  [100 hp][10 attack][3 defence][10 damage]\t\t       ██\n" +
                        "██        *special ability* - gives ...                                \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par4 + "_|  ██\n" +
                        "██   " +
                        "5 - Tank " + FLEXED_BICEPS_SIGN + "\t [150 hp][10 attack][10 defence][15 damage]\t               ██\n" +
                        "██       *special ability* -                                          \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par5 + "_|  ██\n" +
                        "██   Enter numbers of units you want to add to your team                       ██\n" +
                        "██                            Press 9 to exit                                  ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    @SuppressWarnings("Duplicates")
    public void printTeam2Selecting(String par1, String par2, String par3, String par4, String par5){

        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██" +
                        ANSI_RED + "   ________  _______ __________   ___     _____________   __  ___   ________ " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_RED + "  / ___/ _ \\/ __/ _ /_  __/ __/  / _ |   /_  __/ __/ _ | /  |/  /  /  _/  _/ " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_RED + " / /__/ , _/ _// __ |/ / / _/   / __ |    / / / _// __ |/ /|_/ /   / / / /   " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_RED + " \\___/_/|_/___/_/ |_/_/ /___/  /_/ |_|   /_/ /___/_/ |_/_/  /_/  _/ /_/ /    " + ANSI_RESET + "██\n" +
                        "██" +
                        ANSI_RED + "                                                                /___/___/    " + ANSI_RESET + "██\n" +
                        "██                                                                             ██\n" +
                        "██   " +
                        "1 - Warrior " + SWORD_SIGN + "  [100 hp][20 attack][5 defence][30 damage]\t               ██\n" +
                        "██       *special ability* - super kick...                             \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par1 + "_|  ██\n" +
                        "██   " +
                        "2 - Healler " + HELMET_WITH_CROSS_SIGN + "  [80 hp][5 attack][1 defence][5 damage]\t\t               ██\n" +
                        "██       *special ability* - heals ...         \t\t               \t\t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par2 + "_|  ██\n" +
                        "██   " +
                        "3 - Magician " + MAGE_SIGN + "  [80 hp][10 attack][1 defence][10 damage]            \t   ██\n" +
                        "██       *special ability* - poison...                                 \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par3 + "_|  ██\n" +
                        "██   " +
                        "4 - Supporter " + SHIELD_SIGN + "  [100 hp][10 attack][3 defence][10 damage]\t\t       ██\n" +
                        "██        *special ability* - gives ...                                \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par4 + "_|  ██\n" +
                        "██   " +
                        "5 - Tank " + FLEXED_BICEPS_SIGN + "\t [150 hp][10 attack][10 defence][15 damage]\t               ██\n" +
                        "██       *special ability* -                                          \t|¯¯¯|  ██\n" +
                        "██                                                              \t\t|_" + par5 + "_|  ██\n" +
                        "██   Enter numbers of units you want to add to your team                       ██\n" +
                        "██                            Press 9 to exit                                  ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    public void printReady3() {
        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██████╗ ███████╗ █████╗ ██████╗ ██╗   ██╗██████╗    " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██╔══██╗██╔════╝██╔══██╗██╔══██╗╚██╗ ██╔╝╚════██╗   " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██████╔╝█████╗  ███████║██║  ██║ ╚████╔╝   ▄███╔╝   " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██╔══██╗██╔══╝  ██╔══██║██║  ██║  ╚██╔╝    ▀▀══╝    " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██║  ██║███████╗██║  ██║██████╔╝   ██║     ██╗      " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═════╝    ╚═╝     ╚═╝      " + ANSI_RESET + "             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██            $$$$$$\\                                                          ██\n" +
                        "██           $$ ___$$\\                                                         ██ \n" +
                        "██           \\_/   $$ |                                                        ██\n" +
                        "██             $$$$$ /                                                         ██\n" +
                        "██             \\___$$\\                                                         ██\n" +
                        "██           $$\\   $$ |                                                        ██\n" +
                        "██           \\$$$$$$  |                                                        ██\n" +
                        "██            \\______/                                                         ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    public void printReady2() {
        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██████╗ ███████╗ █████╗ ██████╗ ██╗   ██╗██████╗    " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██╔══██╗██╔════╝██╔══██╗██╔══██╗╚██╗ ██╔╝╚════██╗   " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██████╔╝█████╗  ███████║██║  ██║ ╚████╔╝   ▄███╔╝   " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██╔══██╗██╔══╝  ██╔══██║██║  ██║  ╚██╔╝    ▀▀══╝    " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██║  ██║███████╗██║  ██║██████╔╝   ██║     ██╗      " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═════╝    ╚═╝     ╚═╝      " + ANSI_RESET + "             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██            $$$$$$\\         $$$$$$\\                                          ██\n" +
                        "██           $$ ___$$\\       $$  __$$\\                                         ██\n" +
                        "██           \\_/   $$ |      \\__/  $$ |                                        ██\n" +
                        "██             $$$$$ /        $$$$$$  |                                        ██\n" +
                        "██             \\___$$\\       $$  ____/                                         ██\n" +
                        "██           $$\\   $$ |      $$ |                                              ██\n" +
                        "██           \\$$$$$$  |      $$$$$$$$\\                                         ██\n" +
                        "██            \\______/       \\________|                                        ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    public void printReady1() {
        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██████╗ ███████╗ █████╗ ██████╗ ██╗   ██╗██████╗    " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██╔══██╗██╔════╝██╔══██╗██╔══██╗╚██╗ ██╔╝╚════██╗   " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██████╔╝█████╗  ███████║██║  ██║ ╚████╔╝   ▄███╔╝   " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██╔══██╗██╔══╝  ██╔══██║██║  ██║  ╚██╔╝    ▀▀══╝    " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ██║  ██║███████╗██║  ██║██████╔╝   ██║     ██╗      " + ANSI_RESET + "             ██\n" +
                        "██          " +
                        ANSI_PURPLE + "  ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═════╝    ╚═╝     ╚═╝      " + ANSI_RESET + "             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██            $$$$$$\\         $$$$$$\\          $$\\                             ██\n" +
                        "██           $$ ___$$\\       $$  __$$\\       $$$$ |                            ██\n" +
                        "██           \\_/   $$ |      \\__/  $$ |      \\_$$ |                            ██\n" +
                        "██             $$$$$ /        $$$$$$  |        $$ |                            ██\n" +
                        "██             \\___$$\\       $$  ____/         $$ |                            ██\n" +
                        "██           $$\\   $$ |      $$ |              $$ |                            ██\n" +
                        "██           \\$$$$$$  |      $$$$$$$$\\       $$$$$$\\ $$\\ $$\\ $$\\               ██\n" +
                        "██            \\______/       \\________|      \\______|\\__|\\__|\\__|              ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    public void printFirstPlayerWin(){
        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██     " +
                        ANSI_BLUE + "    ______________  ___________   ____  __    _____  ____________" + ANSI_RESET + "       ██\n" +
                        "██     " +
                        ANSI_BLUE + "   / ____/  _/ __ \\/ ___/_  __/  / __ \\/ /   /   \\ \\/ / ____/ __ \\" + ANSI_RESET + "      ██\n" +
                        "██     " +
                        ANSI_BLUE + "  / /_   / // /_/ /\\__ \\ / /    / /_/ / /   / /| |\\  / __/ / /_/ / " + ANSI_RESET + "     ██\n" +
                        "██     " +
                        ANSI_BLUE + " / __/ _/ // _, _/___/ // /    / ____/ /___/ ___ |/ / /___/ _, _/ " + ANSI_RESET + "      ██\n" +
                        "██     " +
                        ANSI_BLUE + "/_/   /___/_/ |_|/____//_/    /_/   /_____/_/  |_/_/_____/_/ |_|  " + ANSI_RESET + "      ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██      " +
                        ANSI_BLUE + "               _       _______   __     " + ANSI_RESET + "                               ██\n" +
                        "██       " +
                        ANSI_BLUE + "             | |     / /  _/ | / /      " + ANSI_RESET + "                              ██\n" +
                        "██        " +
                        ANSI_BLUE + "            | | /| / // //  |/ /        " + ANSI_RESET + "                             ██\n" +
                        "██        " +
                        ANSI_BLUE + "            | |/ |/ // // /|  /         " + ANSI_RESET + "                             ██\n" +
                        "██        " +
                        ANSI_BLUE + "            |__/|__/___/_/ |_/          " + ANSI_RESET + "                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }

    public void printSecondPlayerWin(){
        System.out.println(
                "█████████████████████████████████████████████████████████████████████████████████\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██  " +
                        ANSI_RED + "  _____ ________________  _   ______     ____  __    _____  ____________ " + ANSI_RESET + "  ██\n" +
                        "██  " +
                        ANSI_RED + " / ___// ____/ ____/ __ \\/ | / / __ \\   / __ \\/ /   /   \\ \\/ / ____/ __ \\" + ANSI_RESET + "  ██\n" +
                        "██  " +
                        ANSI_RED + " \\__ \\/ __/ / /   / / / /  |/ / / / /  / /_/ / /   / /| |\\  / __/ / /_/ / " + ANSI_RESET + " ██\n" +
                        "██  " +
                        ANSI_RED + "___/ / /___/ /___/ /_/ / /|  / /_/ /  / ____/ /___/ ___ |/ / /___/ _, _/  " + ANSI_RESET + " ██\n" +
                        "██ "+
                        ANSI_RED + "/____/_____/\\____/\\____/_/ |_/_____/  /_/   /_____/_/  |_/_/_____/_/ |_| " + ANSI_RESET + "   ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██         " + ANSI_RED + "            _       _______   __    " + ANSI_RESET + "                                ██\n" +
                        "██         " + ANSI_RED + "           | |     / /  _/ | / /    " + ANSI_RESET + "                                ██\n" +
                        "██         " + ANSI_RED + "           | | /| / // //  |/ /     " + ANSI_RESET + "                                ██\n" +
                        "██         " + ANSI_RED + "           | |/ |/ // // /|  /      " + ANSI_RESET + "                                ██\n" +
                        "██         " + ANSI_RED + "           |__/|__/___/_/ |_/       " + ANSI_RESET + "                                ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "██                                                                             ██\n" +
                        "█████████████████████████████████████████████████████████████████████████████████"
        );
    }
}
