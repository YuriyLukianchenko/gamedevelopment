package com.epam.view.animation;

import com.epam.model.units.*;

public class BattleFieldCreator {
    /**
     * Uses ths list of players 1 and player 2 and
     * creates battle field with appropriate units
     */
    public void unitPlaceInitialization() {
       /* StartAnimation.firstPlayerUnits.add(new DamageDealer());
        StartAnimation.firstPlayerUnits.add(new Tank());
        StartAnimation.firstPlayerUnits.add(new Supporter());
        StartAnimation.firstPlayerUnits.add(new Magician());
        StartAnimation.firstPlayerUnits.add(new Healer());
        StartAnimation.secondPlayerUnits.add(new Magician());
        StartAnimation.secondPlayerUnits.add(new Healer());
        StartAnimation.secondPlayerUnits.add(new Tank());
        StartAnimation.secondPlayerUnits.add(new Supporter());
        StartAnimation.secondPlayerUnits.add(new DamageDealer());*/
        StartAnimation.fieldsList.add(fieldInitialization(StartAnimation.firstPlayerUnits.get(0), StartAnimation.secondPlayerUnits.get(0)));
        StartAnimation.fieldsList.add(fieldInitialization(StartAnimation.firstPlayerUnits.get(1), StartAnimation.secondPlayerUnits.get(1)));
        StartAnimation.fieldsList.add(fieldInitialization(StartAnimation.firstPlayerUnits.get(2), StartAnimation.secondPlayerUnits.get(2)));
        StartAnimation.fieldsList.add(fieldInitialization(StartAnimation.firstPlayerUnits.get(3), StartAnimation.secondPlayerUnits.get(3)));
        StartAnimation.fieldsList.add(fieldInitialization(StartAnimation.firstPlayerUnits.get(4), StartAnimation.secondPlayerUnits.get(4)));
    }

    /**
     * One field consists of model of unit in the left corner
     * model in the right corner and tabulation between them
     * @param player1 unit in the left corner
     * @param player2 unit in the right corner
     * @return initialized field
     */
    private String[][] fieldInitialization(Unit player1, Unit player2) {
        String[][] array = new String[4][11];
        array[0][0] = "\t";
        array[1][0] = "\t";
        array[2][0] = "\t";
        array[3][0] = "\t";
        //model of left unit
        array[0][1] = player1.getHead();
        array[1][1] = player1.getArms();
        array[2][1] = player1.getBelly();
        array[3][1] = player1.getLegs();
        //just a loop for filling in tabulations
        for (int i = 0; i < array.length; i++) {
            for (int j = 2; j < array[i].length - 2; j++) {
                array[i][j] = "\t";
            }
        }
        //model of right unit
        array[0][9] = player2.getHead();
        array[1][9] = player2.getArms();
        array[2][9] = player2.getBelly();
        array[3][9] = player2.getLegs();
        array[0][10] = "\t";
        array[1][10] = "\t";
        array[2][10] = "\t";
        array[3][10] = "\t";
        return array;
    }
}
