package com.epam.view.animation;

import static com.epam.view.animation.StartAnimation.*;

public class ModelAnimation {
    /**
     * Shows animation of damage IF a unit is not dead
     * @param defenceField it is a field on which stands a unit
     *                     whom you are attacking
     */
    public void damageAnimation(String[][] defenceField) {
        if (!(deathAnimation(defenceField))) {
            if (playerMove == 1) {
                defenceField[0][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getDamagedHead();
                defenceField[1][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getDamagedArms();
                defenceField[2][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getDamagedBelly();
                defenceField[3][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getDamagedLegs();
                new BattleField().printBattleField();
                defenceField[0][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getHead();
                defenceField[1][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getArms();
                defenceField[2][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getBelly();
                defenceField[3][9] = StartAnimation.secondPlayerUnits.get(enemyChoice - 1).getLegs();
            } else {
                defenceField[0][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getDamagedHead();
                defenceField[1][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getDamagedArms();
                defenceField[2][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getDamagedBelly();
                defenceField[3][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getDamagedLegs();
                new BattleField().printBattleField();
                defenceField[0][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getHead();
                defenceField[1][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getArms();
                defenceField[2][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getBelly();
                defenceField[3][1] = StartAnimation.firstPlayerUnits.get(enemyChoice - 1).getLegs();
            }
        }
    }

    /**
     * Validation whether a unit
     * whom you are attacking is dead
     * @param defenceField it is a field on which stands a unit
     *                     whom you are attacking
     * @return validation
     */
    private boolean deathAnimation(String[][] defenceField) {
        if (!(StartAnimation.firstPlayerUnits.get(enemyChoice - 1).isAlive()) && !(StartAnimation.secondPlayerUnits.get(enemyChoice - 1).isAlive())) {
            defenceField[0][1] = "\t";
            defenceField[1][1] = "\t";
            defenceField[2][1] = "\t";
            defenceField[3][1] = "\t";
            defenceField[0][9] = "\t";
            defenceField[1][9] = "\t";
            defenceField[2][9] = "\t";
            defenceField[3][9] = "\t";
            return true;
        } else if (!(StartAnimation.firstPlayerUnits.get(enemyChoice - 1).isAlive())) {
            defenceField[0][1] = "\t";
            defenceField[1][1] = "\t";
            defenceField[2][1] = "\t";
            defenceField[3][1] = "\t";
            return true;
        } else if (!(StartAnimation.secondPlayerUnits.get(enemyChoice - 1).isAlive())) {
            defenceField[0][9] = "\t";
            defenceField[1][9] = "\t";
            defenceField[2][9] = "\t";
            defenceField[3][9] = "\t";
            return true;
        } else {
            return false;
        }
    }

    /**
     * Adds cursor in front of an attacking unit
     */
    public void cursorAnimation() {
        String[][] currentField = fieldsList.get(StartAnimation.currentField - 1);
        if (playerMove == 1) {
            currentField[0][0] = "->  ";
            currentField[1][0] = "->  ";
            currentField[2][0] = "->  ";
            currentField[3][0] = "->  ";
        } else if (playerMove == 2) {
            currentField[0][10] = "  <-";
            currentField[1][10] = "  <-";
            currentField[2][10] = "  <-";
            currentField[3][10] = "  <-";
        }
    }

    /**
     * Erases cursor from a previous unit
     */
    public void erasePreviousCursor() {
        String[][] currentField = fieldsList.get(StartAnimation.currentField - 1);
        if (playerMove == 1) {
            currentField[0][0] = "\t";
            currentField[1][0] = "\t";
            currentField[2][0] = "\t";
            currentField[3][0] = "\t";
        } else if (playerMove == 2) {
            currentField[0][10] = "\t";
            currentField[1][10] = "\t";
            currentField[2][10] = "\t";
            currentField[3][10] = "\t";
        }
    }
}
