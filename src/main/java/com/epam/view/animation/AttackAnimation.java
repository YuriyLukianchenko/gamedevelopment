package com.epam.view.animation;

import com.epam.model.battleActions.DamageCalculatorV1;

import static com.epam.view.animation.StartAnimation.currentField;
import static com.epam.view.animation.StartAnimation.enemyChoice;

public class AttackAnimation {
    /**
     * A left unit from attackField(whose turn to attack) goes on defenceField(whom you chose to attack),
     * moves forward, invokes damage calculation method,
     * appears damage animation or death animation(if a unit from defenceField is dead),
     * moves backward, returns on his own attackField, invokes method of printing battle field
     * @param attackField it is a field on which stands a unit
     *                     who is attacking
     * @param defenceField it is a field on which stands a unit
     *                     whom you are attacking
     */
    public void attackAnimationFirstPlayer(String[][] attackField, String[][] defenceField) {
        // loop of changing a field
        for (int i = 0; i <= 3; i++) {
            defenceField[i][2] = attackField[i][1];
            attackField[i][1] = "\t";
        }
        new BattleField().printBattleField();
        moveForward(defenceField);
        new DamageCalculatorV1().calculateDamage(StartAnimation.firstPlayerUnits.get(currentField - 1), StartAnimation.secondPlayerUnits.get(enemyChoice - 1));
        new ModelAnimation().damageAnimation(defenceField);
        moveBackward(defenceField);
        // loop of returning to unit`s own field
        for (int i = 0; i <= 3; i++) {
            attackField[i][1] = defenceField[i][2];
            defenceField[i][2] = "\t";
        }
        new ModelAnimation().erasePreviousCursor();
    }
    /**
     * A right unit from attackField(whose turn to attack) goes on defenceField(whom you chose to attack),
     * moves forward, invokes damage calculation method,
     * appears damage animation or death animation(if a unit from defenceField is dead),
     * moves backward, returns on his own attackField, invokes method of printing battle field
     * @param attackField it is a field on which stands a unit
     *                     who is attacking
     * @param defenceField it is a field on which stands a unit
     *                     whom you are attacking
     */
    public void attackAnimationSecondPlayer(String[][] attackField, String[][] defenceField) {
        // loop of changing a field
        for (int i = 0; i <= 3; i++) {
            defenceField[i][8] = attackField[i][9];
            attackField[i][9] = "\t";
        }
        new BattleField().printBattleField();
        moveBackward(defenceField);
        new DamageCalculatorV1().calculateDamage(StartAnimation.secondPlayerUnits.get(currentField - 1), StartAnimation.firstPlayerUnits.get(enemyChoice - 1));
        new ModelAnimation().damageAnimation(defenceField);
        moveForward(defenceField);
        // loop of returning to unit`s own field
        for (int i = 0; i <= 3; i++) {
            attackField[i][9] = defenceField[i][8];
            defenceField[i][8] = "\t";
        }
        new ModelAnimation().erasePreviousCursor();
    }

    /**
     * StartMenu of moving an attacking unit forward
     * @param defenceField it is a field on which stands a unit
     *                     whom you are attacking
     */
    private void moveForward(String[][] defenceField) {
        for (int j = 2; j <= 7; j++) {
            for (int i = 0; i <= 3; i++) {
                defenceField[i][j + 1] = defenceField[i][j];
                defenceField[i][j] = "\t";
            }
            new BattleField().printBattleField();
        }
    }

    /**
     * StartMenu of moving an attacking unit backward
     * @param defenceField it is a field on which stands a unit
     *                     whom you are attacking
     */
    private void moveBackward(String[][] defenceField) {
        for (int j = 8; j >= 3; j--) {
            for (int i = 0; i <= 3; i++) {
                defenceField[i][j - 1] = defenceField[i][j];
                defenceField[i][j] = "\t";
            }
            new BattleField().printBattleField();
        }
    }
}
