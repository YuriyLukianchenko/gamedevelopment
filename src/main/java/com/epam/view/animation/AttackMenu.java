package com.epam.view.animation;

import com.epam.model.units.DamageDealer;
import java.util.Scanner;

import static com.epam.view.animation.StartAnimation.*;

public class AttackMenu {
    private Scanner scanner = new Scanner(System.in);
    /**
     * In this method you choose between attack and special ability
     * and then if you chose attack you print a number of enemy you
     * wanna attack, when you chose special ability it invokes an overridden
     * corresponding method in particular unit class
     * @param attackField it is a field on which stands a unit
     *                     who is attacking
     */
    public void playerAttackMenu(String[][] attackField) {
        System.out.println("1 - Attack");
        System.out.println("2 - Special ability");
        int attackChoice = scanner.nextInt();
        deadUnit = true;
        if (attackChoice == 1) {
            while (deadUnit) {
                System.out.println("Choose your enemy");
                enemyChoice = scanner.nextInt();
                if (enemyChoice == 1) {
                    choosePlayerMove(attackField);
                } else if (enemyChoice == 2) {
                    choosePlayerMove(attackField);
                } else if (enemyChoice == 3) {
                    choosePlayerMove(attackField);
                } else if (enemyChoice == 4) {
                    choosePlayerMove(attackField);
                } else if (enemyChoice == 5) {
                    choosePlayerMove(attackField);
                }
            }
        } else if (attackChoice == 2) {
            if (playerMove == 1) {
                if (StartAnimation.firstPlayerUnits.get(currentField - 1) instanceof DamageDealer) {
                    StartAnimation.firstPlayerUnits.get(currentField - 1).specialAbility(secondPlayerUnits);
                    new ModelAnimation().erasePreviousCursor();
                    playerMove = 2;
                } else {
                    StartAnimation.firstPlayerUnits.get(currentField - 1).specialAbility(firstPlayerUnits);
                    new ModelAnimation().erasePreviousCursor();
                    playerMove = 2;
                }
            } else if (playerMove == 2) {
                if (StartAnimation.secondPlayerUnits.get(currentField - 1) instanceof DamageDealer) {
                    StartAnimation.secondPlayerUnits.get(currentField - 1).specialAbility(firstPlayerUnits);
                    new ModelAnimation().erasePreviousCursor();
                    playerMove = 1;
                } else {
                    StartAnimation.secondPlayerUnits.get(currentField - 1).specialAbility(secondPlayerUnits);
                    new ModelAnimation().erasePreviousCursor();
                    playerMove = 1;
                }
            }
        }
    }

    /**
     * Depending on a parameter playerMove this method decides which
     * method to invoke, meleeAttackFirstPlayer if attacks a left unit
     * or meleeAttackSecondPlayer if attacks a right unit,
     * or if you attacked a dead unit just omit him
     * @param attackField it is a field on which stands a unit
     *                     who is attacking
     */
    private void choosePlayerMove(String[][] attackField) {
        if (playerMove == 1 && StartAnimation.secondPlayerUnits.get(enemyChoice - 1).isAlive()) {
            new AttackAnimation().attackAnimationFirstPlayer(attackField, StartAnimation.fieldsList.get(enemyChoice - 1));
            playerMove = 2;
            deadUnit = false;
        } else if (playerMove == 2 && StartAnimation.firstPlayerUnits.get(enemyChoice - 1).isAlive()) {
            new AttackAnimation().attackAnimationSecondPlayer(attackField, StartAnimation.fieldsList.get(enemyChoice - 1));
            playerMove = 1;
            deadUnit = false;
        } else {
            System.out.println("This unit is already dead");
            /*
             * this parameter makes possible for you to continue choosing your enemy
             * if you chose dead unit
             */
            deadUnit = true;
        }
    }
}
