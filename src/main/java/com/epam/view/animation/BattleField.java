package com.epam.view.animation;

import com.epam.view.menu.defaults.DefaultMenuMessages;

public class BattleField {
    DefaultMenuMessages defaultMenuMessages = new DefaultMenuMessages();

    /**
     * The basic method of the whole class. This is the method you need to frame Andriy.
     * This method prints hp above unit depends on whether one unit is alive, or
     * two of them are dead or two are alive. Invokes printField method 5 times,
     * so you can see 5 rows of units.
     * And of course delay for animation (you can adjust the time whatever you like)
     */
    public void printBattleField() {
        defaultMenuMessages.printHorizontalBorder();
        System.out.print("███\n");
        for (int i = 0; i <= 4; i++) {
            if (!(StartAnimation.firstPlayerUnits.get(i).isAlive()) && !(StartAnimation.secondPlayerUnits.get(i).isAlive())) {
                //both units left and right are dead (prints blank space)
                System.out.println();
            } else if (!(StartAnimation.firstPlayerUnits.get(i).isAlive())) {
                //left unit is dead, prints hp only for the right one

                System.out.println("\t\t\t\t\t\t\t\t" + "HP = " + StartAnimation.secondPlayerUnits.get(i).getHp());
            } else if (!(StartAnimation.secondPlayerUnits.get(i).isAlive())) {
                System.out.print(defaultMenuMessages.BORDER + defaultMenuMessages.BORDER);
                System.out.print("              ");
                //right unit is dead, prints hp only for the left one
                System.out.println("HP = " + StartAnimation.firstPlayerUnits.get(i).getHp());
            } else {
                //both left and right are alive, prints hp for two of them
                System.out.print(defaultMenuMessages.BORDER + defaultMenuMessages.BORDER);
                System.out.print("              ");
                System.out.print("HP = " + StartAnimation.firstPlayerUnits.get(i).getHp() + "\t\t\t\t\t\t\t"
                        + "HP = " + StartAnimation.secondPlayerUnits.get(i).getHp());
                System.out.print("                     ██\n");

            }
            printField(StartAnimation.fieldsList.get(i));
        }
        defaultMenuMessages.printHorizontalBorder();
        System.out.print("███\n");
        delay();
    }

    /**
     * This method prints one field
     * @param field one model of unit standing in front of
     *              the other is called one field
     */
    private void printField(String[][] field) {
        for (int i = 0; i < field.length; i++) {
            System.out.print(defaultMenuMessages.BORDER + defaultMenuMessages.BORDER);
            System.out.print("              ");
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j]);
            }
            System.out.print("                     ");
            System.out.print(defaultMenuMessages.BORDER + defaultMenuMessages.BORDER);
            System.out.println();

        }
        System.out.println("██                                                                               ██");
    }

    private void delay() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
