package com.epam.view.animation;

import com.epam.model.units.*;
import com.epam.view.menu.defaults.DefaultMenuMessages;
import java.util.ArrayList;
import java.util.List;

public class StartAnimation {
    //list with units for the left team
    public static List<Unit> firstPlayerUnits = new ArrayList<>();
    //list with units for the right team
    public static List<Unit> secondPlayerUnits = new ArrayList<>();
    //list of field composes battle field
    public static List<String[][]> fieldsList = new ArrayList<>();
    // if playerMove = 1 left unit attacks, if playerMove = 2, right unit
    // default playerMove = 1, so left unit always start the battle
    public static int playerMove = 1;
    //you print what enemy to attack from 1 to 5
    public static int enemyChoice;
    //current field's row can be from 1 to 5
    public static int currentField;
    //caveat from attacking dead unit
    public static boolean deadUnit = true;

    /**
     * Main method which starts the program.
     */
    public void startProgram() {
        new BattleFieldCreator().unitPlaceInitialization();
        startGamePlay();
    }

    /**
     * While all units of one or the other team are alive the loop works.
     * Invokes method playerAttackMenu, if a unit is dead, "if/else" condition omits him
     * and player move goes to the opposite unit.
     * Also prints the message for the victory team and ends the program
     */
    public void startGamePlay() {
        DefaultMenuMessages defaultMenuMessages = new DefaultMenuMessages();
        while (true) {
            currentField = 1;
            for (int i = 0; i < 5; i++) {
                if (firstPlayerUnits.get(i).isAlive()) {
                    new ModelAnimation().cursorAnimation();
                    new BattleField().printBattleField();
                    new AttackMenu().playerAttackMenu(fieldsList.get(i));
                } else {
                    playerMove = 2;
                }
                if (secondPlayerUnits.get(i).isAlive()) {
                    new ModelAnimation().cursorAnimation();
                    new BattleField().printBattleField();
                    new AttackMenu().playerAttackMenu(fieldsList.get(i));
                } else {
                    playerMove = 1;
                }
                currentField++;
                if (!(firstPlayerUnits.get(0).isAlive()) && !(firstPlayerUnits.get(1).isAlive())
                        && !(firstPlayerUnits.get(2).isAlive()) && !(firstPlayerUnits.get(3).isAlive())
                        && !(firstPlayerUnits.get(4).isAlive())) {
                    defaultMenuMessages.printSecondPlayerWin();
                    System.exit(0);
                } else if (!(secondPlayerUnits.get(0).isAlive()) && !(secondPlayerUnits.get(1).isAlive())
                        && !(secondPlayerUnits.get(2).isAlive()) && !(secondPlayerUnits.get(3).isAlive())
                        && !(secondPlayerUnits.get(4).isAlive())) {
                    defaultMenuMessages.printFirstPlayerWin();
                    System.exit(0);
                }
            }
        }
    }
}
