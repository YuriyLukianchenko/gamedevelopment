package com.epam;

import com.epam.view.animation.StartAnimation;
import com.epam.view.menu.Menu;

public class StartMenu {
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.printMenu();

        StartAnimation start = new StartAnimation();
        start.firstPlayerUnits = menu.ArrayOfUnits1;
        start.secondPlayerUnits = menu.ArrayOfUnits2;

        start.startProgram();
    }
}
