package com.epam.model.units;

import java.util.List;

public abstract class Unit {
    private int hp;
    private int attack;
    private int defence;
    private int damage;
    protected String head;
    protected String arms;
    protected String belly;
    protected String legs;
    protected String damagedHead;
    protected String damagedArms;
    protected String damagedBelly;
    protected String damagedLegs;
    protected boolean isAlive;

    public Unit(int hp, int attack, int defence, int damage) {
        this.hp = hp;
        this.attack = attack;
        this.defence = defence;
        this.damage = damage;
    }

    public abstract void specialAbility(List<Unit> playerUnits);

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getArms() {
        return arms;
    }

    public void setArms(String arms) {
        this.arms = arms;
    }

    public String getBelly() {
        return belly;
    }

    public void setBelly(String belly) {
        this.belly = belly;
    }

    public String getLegs() {
        return legs;
    }

    public void setLegs(String legs) {
        this.legs = legs;
    }

    public String getDamagedHead() {
        return damagedHead;
    }

    public void setDamagedHead(String damagedHead) {
        this.damagedHead = damagedHead;
    }

    public String getDamagedArms() {
        return damagedArms;
    }

    public void setDamagedArms(String damagedArms) {
        this.damagedArms = damagedArms;
    }

    public String getDamagedBelly() {
        return damagedBelly;
    }

    public void setDamagedBelly(String damagedBelly) {
        this.damagedBelly = damagedBelly;
    }

    public String getDamagedLegs() {
        return damagedLegs;
    }

    public void setDamagedLegs(String damagedLegs) {
        this.damagedLegs = damagedLegs;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
