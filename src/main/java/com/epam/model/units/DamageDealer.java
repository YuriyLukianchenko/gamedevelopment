package com.epam.model.units;

import com.epam.view.animation.BattleField;
import com.epam.view.animation.StartAnimation;

import java.util.List;

import static com.epam.view.animation.StartAnimation.playerMove;

public class DamageDealer extends Unit {

    public DamageDealer() {
        super(100, 12, 5, 13);
        isAlive = true;
        head = " () /";
        arms = "/|\\/";
        belly = " |\t";
        legs = " /\\\t";
        damagedHead = " *() /*";
        damagedArms = "*/|\\/*";
        damagedBelly = " *|\t";
        damagedLegs = " */\\\t";
    }

    @Override
    public void specialAbility(List<Unit> playerUnits) {
        String[][] currentField;
        for (int i = 0; i < playerUnits.size(); i++) {
            if (playerUnits.get(i).isAlive) {
                playerUnits.get(i).setHp(playerUnits.get(i).getHp() - 10);
                currentField = StartAnimation.fieldsList.get(i);
                if (playerMove == 1) {
                    currentField[0][7] = "-10 HP";
                    currentField[1][7] = "-10 HP";
                    currentField[2][7] = "-10 HP";
                    currentField[3][7] = "-10 HP";
                    new BattleField().printBattleField();
                    currentField[0][7] = "\t";
                    currentField[1][7] = "\t";
                    currentField[2][7] = "\t";
                    currentField[3][7] = "\t";
                } else if (playerMove == 2) {
                    currentField[0][3] = "-10 HP";
                    currentField[1][3] = "-10 HP";
                    currentField[2][3] = "-10 HP";
                    currentField[3][3] = "-10 HP";
                    new BattleField().printBattleField();
                    currentField[0][3] = "\t";
                    currentField[1][3] = "\t";
                    currentField[2][3] = "\t";
                    currentField[3][3] = "\t";
                }
            }
        }
    }
}
