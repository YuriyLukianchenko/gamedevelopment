package com.epam.model.units;

import com.epam.view.animation.BattleField;
import com.epam.view.animation.StartAnimation;

import java.util.List;

import static com.epam.view.animation.StartAnimation.playerMove;

public class Tank extends Unit {

    public Tank() {
        super(150, 12, 6, 12);
        isAlive = true;
        head = " ()\t";
        arms = "/|\\||";
        belly = " | ||";
        legs = " /\\\t";
        damagedHead = " *()*";
        damagedArms = "*/|\\||*";
        damagedBelly = " *| ||*";
        damagedLegs = " */\\*";
    }

    @Override
    public void specialAbility(List<Unit> playerUnits) {
        String[][] currentField;
        for (int i = 0; i < playerUnits.size(); i++) {
            if (playerUnits.get(i).isAlive) {
                playerUnits.get(i).setDefence(playerUnits.get(i).getDefence() + 3);
                currentField = StartAnimation.fieldsList.get(i);
                if (playerMove == 1) {
                    currentField[0][3] = "+3 Defence";
                    currentField[1][3] = "+3 Defence";
                    currentField[2][3] = "+3 Defence";
                    currentField[3][3] = "+3 Defence";
                    new BattleField().printBattleField();
                    currentField[0][3] = "\t";
                    currentField[1][3] = "\t";
                    currentField[2][3] = "\t";
                    currentField[3][3] = "\t";
                } else if (playerMove == 2) {
                    currentField[0][7] = "+3 Defence";
                    currentField[1][7] = "+3 Defence";
                    currentField[2][7] = "+3 Defence";
                    currentField[3][7] = "+3 Defence";
                    new BattleField().printBattleField();
                    currentField[0][7] = "\t";
                    currentField[1][7] = "\t";
                    currentField[2][7] = "\t";
                    currentField[3][7] = "\t";
                }
            }
        }
    }
}
