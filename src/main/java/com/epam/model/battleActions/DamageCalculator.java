package com.epam.model.battleActions;

import com.epam.model.units.Unit;

public interface DamageCalculator {
    public void calculateDamage(Unit unitActive, Unit unitReciever);
    public void performAbility(int typeOfAction);
}
