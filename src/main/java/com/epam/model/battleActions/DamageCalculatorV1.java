package com.epam.model.battleActions;

import com.epam.model.units.Unit;

public class DamageCalculatorV1 implements DamageCalculator {

    private int hpTransit;
    private int attackTransit;
    private int deffenceTransit;
    private int damageTransit;

    public void calculateDamage(Unit unitActive, Unit unitReciever){
        hpTransit = unitReciever.getHp();
        attackTransit = unitActive.getAttack();
        deffenceTransit = unitReciever.getDefence();
        damageTransit = unitActive.getDamage();

        hpTransit -= damageTransit*(attackTransit/deffenceTransit);
        unitReciever.setHp(hpTransit);
        if (unitReciever.getHp() <= 0) {
            unitReciever.setAlive(false);
        }
    }

    public void performAbility(int typeOfAction){

    }
}
